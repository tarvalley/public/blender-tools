"""Operator allowing easier skinning objects to armature bones"""

import bpy
from bpy.types import Operator, Mesh
from bpy.props import StringProperty


class SkinMeshToBone(Operator):
    """Adds an Armature modifier with the given armature assigned to the object and
assign all vertices to a new Vertex Group with the given bone name"""

    bl_idname = "mesh.skin_mesh_to_bone"
    bl_label = "Skin Mesh to Bone"

    target_armature: StringProperty(name="Target Armature", default="Armature")
    target_bone: StringProperty(name="Target Bone", default="")

    @classmethod
    def poll(cls, context):
        for obj in context.selected_objects:
            if isinstance(obj.data, Mesh):
                return True
        return False

    def execute(self, context):
        for obj in context.selected_objects:
            if not isinstance(obj.data, Mesh):
                continue

            armature = bpy.data.objects[self.target_armature]
            modifier = obj.modifiers.new("Armature", 'ARMATURE')
            modifier.object = armature

            group = obj.vertex_groups.new(name=self.target_bone)
            indices = []
            for vert in obj.data.vertices:
                indices.append(vert.index)
            group.add(indices, 1, 'REPLACE')

        return {'FINISHED'}

    def invoke(self, context, event):
        return context.window_manager.invoke_props_dialog(self)


REGISTER_CLASSES = (
    SkinMeshToBone,
)
