"""Operators for converting object animations to an animated armature"""

import bpy
from bpy.types import Operator, Armature, Mesh
from bpy.props import FloatProperty
from mathutils import Matrix


class ArmatureFromObjects(Operator):
    """Creates an armature bone hierarchy from objects"""

    bl_idname = "object.armature_from_objects"
    bl_label = "Armature From Objects"
    bl_options = {'REGISTER', 'UNDO'}

    bone_scale: FloatProperty(name="Bone Scale", default=1.0)

    @classmethod
    def poll(cls, context):
        return len(context.selected_objects) > 0

    def execute(self, context):
        bone_height = self.bone_scale / context.scene.unit_settings.scale_length

        # save selected objects for later
        selected_objects = context.selected_objects

        # add armature
        bpy.ops.object.add(type='ARMATURE', enter_editmode=True)
        armature = context.object
        armature.name = "Armature"
        armature.show_in_front = True
        armature.data.name = armature.name

        # add root bone
        root_bone = armature.data.edit_bones.new("root")
        root_bone.tail = [0, 0, bone_height]

        # create object bones
        for obj in selected_objects:
            bone = armature.data.edit_bones.new(obj.name)
            bone.tail = [0, 0, bone_height]
            transform = Matrix.Translation(obj.matrix_world.to_translation())
            bone.transform(transform @ armature.matrix_world.inverted())

        # solve hierarchy
        for obj in selected_objects:
            parent = obj.parent
            parent_bone_name = "root"
            while parent is not None:
                if parent in selected_objects:
                    parent_bone_name = parent.name
                    break
                parent = parent.parent

            bone = armature.data.edit_bones[obj.name]
            bone.parent = armature.data.edit_bones[parent_bone_name]

        # make constraints
        bpy.ops.object.mode_set(mode='POSE')
        for bone in armature.pose.bones:
            if bone.name == "root":
                continue
            obj = bpy.data.objects[bone.name]
            location = bone.constraints.new('COPY_LOCATION')
            location.target = obj
            rotation = bone.constraints.new('COPY_ROTATION')
            rotation.target = obj
            scale = bone.constraints.new('COPY_SCALE')
            scale.owner_space = 'LOCAL'
            scale.target_space = 'LOCAL'
            scale.target = obj
        # set rest to constrained transforms
        bpy.ops.pose.armature_apply(selected=False)

        # go back to object mode
        bpy.ops.object.mode_set(mode='OBJECT')

        return {'FINISHED'}

    def invoke(self, context, event):
        return context.window_manager.invoke_props_dialog(self)


class ArmatureDeformObjects(Operator):
    """Creates an armature modifiers for every selected object connected to the active armature.
Also creates a new vertex group for every object. The group will have the name of a bone
that either matches the object or one of its parent"""

    bl_idname = "object.armature_deform_objects"
    bl_label = "Armature Deform Objects"
    bl_options = {'REGISTER', 'UNDO'}

    @classmethod
    def poll(cls, context):
        return len(context.selected_objects) > 0 and isinstance(context.active_object.data, Armature)

    def execute(self, context):
        armature = context.active_object
        bones = armature.data.bones
        objects = context.selected_objects
        objects.remove(armature)
        meshes = list(filter(lambda x: isinstance(x.data, Mesh), objects))

        for obj in meshes:
            current_obj = obj
            bone_name = "root"
            while bone_name == "root" and current_obj is not None:
                for bone in bones:
                    if bone.name == current_obj.name:
                        bone_name = bone.name
                
                current_obj = current_obj.parent

            modifier = obj.modifiers.new("Armature", 'ARMATURE')
            modifier.object = armature

            group = obj.vertex_groups.new(name=bone_name)
            indices = []
            for vert in obj.data.vertices:
                indices.append(vert.index)
            group.add(indices, 1, 'REPLACE')

        return {'FINISHED'}


REGISTER_CLASSES = (
    ArmatureFromObjects,
    ArmatureDeformObjects,
)
