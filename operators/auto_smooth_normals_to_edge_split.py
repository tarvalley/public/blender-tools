"""Operator for converting Auto Smooth Normals to the Edge Split modifier"""

from bpy.types import Operator


class AutoSmoothNormalsToEdgeSplit(Operator):
    """Disables Auto Smooth Normals and copies the angle value to a new Edge Split Modifier"""

    bl_idname = "object.auto_smooth_normals_to_edge_split"
    bl_label = "Auto Smooth Normals to Edge Split"

    @classmethod
    def poll(cls, context):
        return len(context.selected_objects) > 0

    def execute(self, context):
        for obj in context.selected_objects:
            data = obj.data
            try:
                if data.use_auto_smooth:
                    data.use_auto_smooth = False
                    modifier = obj.modifiers.new("Edge Split", 'EDGE_SPLIT')
                    modifier.split_angle = data.auto_smooth_angle
            except AttributeError:
                pass

        return {'FINISHED'}


REGISTER_CLASSES = (
    AutoSmoothNormalsToEdgeSplit,
)
