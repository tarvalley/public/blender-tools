"""Operator for setting objects shading to smooth and enable auto normals"""

import math
import bpy
from bpy.types import Operator
from bpy.props import FloatProperty, BoolProperty


class ShadeAuto(Operator):
    """Sets the objects shading to smooth and enables auto normals"""
    bl_idname = "object.shade_auto"
    bl_label = "Shade Auto"
    bl_options = {'REGISTER', 'UNDO'}

    auto_smooth_angle: FloatProperty(
        name="Auto Smooth Angle", default=30, min=0, max=180)

    override_existing: BoolProperty(name="Override Existing", default=False)

    @classmethod
    def poll(cls, context):
        return len(context.selected_objects) > 0 or context.active_object is not None

    def execute(self, context):
        bpy.ops.object.shade_smooth()

        for obj in context.selected_objects:
            data = obj.data
            try:
                if not data.use_auto_smooth or self.override_existing:
                    data.use_auto_smooth = True
                    data.auto_smooth_angle = self.auto_smooth_angle / 180 * math.pi
            except AttributeError:
                pass

        return {'FINISHED'}


REGISTER_CLASSES = (
    ShadeAuto,
)
