"""Custom Blender Operators that are loaded by default with the Tarvalley Addon.

If you add a new Operator, please define register() and unregister() functions
for it, and then import it in this file and add it to the REGISTER_CLASSES list
"""


from . import (
    auto_smooth_normals_to_edge_split,
    shade_auto,
    skin_mesh_to_bone,
    convert_to_bone_anim,
)


REGISTER_CLASSES = (
    auto_smooth_normals_to_edge_split,
    shade_auto,
    skin_mesh_to_bone,
    convert_to_bone_anim,
)
