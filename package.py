import os
from os import path
import zipfile

VERSION = os.environ.get("CI_COMMIT_REF_NAME")
ARCBASE = "tarvalley-blender-tools"

WHITELIST = [
    "icons",
    "operators",
    "utils",
    "vendor",
    "__init__.py"
]

BLACKLIST = [
    "__pycache__",
]


with zipfile.ZipFile(f"tarvalley-blender-tools_{VERSION}.zip", 'w') as zf:
    for white_entry in WHITELIST:
        if path.isdir(white_entry):
            for root, dirs, files in os.walk(white_entry):
                for file in files:
                    is_black = False
                    for black_entry in BLACKLIST:
                        if black_entry in root:
                            is_black = True
                    if not is_black:
                        file_path = path.join(root, file)
                        zf.write(file_path, path.join(ARCBASE, file_path))
        else:
            zf.write(white_entry, path.join(ARCBASE, white_entry))
