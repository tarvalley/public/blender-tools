"""Quality of life tools, created by Tarvalley.
Repository: https://gitlab.com/tarvalley/public/blender-tools/
Tarvalley: https://www.tarvalley.com"""

import math
import bpy
from .vendor.addon_updater import addon_updater_ops
from .utils import ui, preferences, register_recursive, unregister_recursive
from . import operators

bl_info = {
    "name": "Tarvalley Addon",
    "description": "Quality of life tools, created by Tarvalley",
    "author": "Tarvalley",
    "version": (0, 7, 1),
    "blender": (2, 90, 1),
    "location": "Tarvalley menu",
    "wiki_url": "https://gitlab.com/tarvalley/public/blender-tools/",
    "tracker_url": "https://gitlab.com/tarvalley/public/blender-tools/issues/new",
    "support": "COMMUNITY",
    "category": "Generic"
}


REGISTER_CLASSES = (
    preferences,
    ui,
    operators,
)


def register():
    addon_updater_ops.register(bl_info)

    register_recursive(REGISTER_CLASSES)


def unregister():
    addon_updater_ops.unregister()

    unregister_recursive(REGISTER_CLASSES)
