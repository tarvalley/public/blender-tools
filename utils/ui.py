"""UI functionality for the Tarvalley Addon, including menus and icon management"""

from os import listdir, path
import bpy
from bpy.utils import previews, register_class, unregister_class
from bpy.types import Menu
from .. import operators

__icon_manager__ = None


class IconManager():
    """Singleton class for handling icons in the Blender previews system"""
    icons = None
    _supported_formats = ('.png')

    def __init__(self):
        self.icons = previews.new()

        icons_dir = path.normpath(
            path.join(path.dirname(__file__), "../icons"))
        for icon_file in sorted(listdir(icons_dir)):
            name_tokens = path.splitext(icon_file)
            filepath = path.join(icons_dir, icon_file)
            if name_tokens[1] in self._supported_formats:
                self.icons.load(name_tokens[0], filepath, 'IMAGE')
            else:
                print(
                    f"Error: Unsupported icon format '{name_tokens[1]}': {filepath}")

    def unregister(self):
        """Remove the Tarvalley Addon's icon previews from Blender"""
        previews.remove(self.icons)
        self.icons = None


def get_icon(name):
    """Get an internal Blender icon ID from an icon name"""
    try:
        return __icon_manager__.icons[name].icon_id
    except KeyError:
        print(f"Error: Failed to find icon named '{name}'!")
        return None


class ConvertToBoneAnimMenu(Menu):
    """A menu for any registered functionality that should be accessible in any context"""
    bl_idname = "TOPBAR_MT_ConvertToBoneAnimMenu"
    bl_label = "Convert to Bone Anim"

    def draw(self, context):
        """Draw the menu"""
        self.layout.operator(
            operators.convert_to_bone_anim.ArmatureFromObjects.bl_idname)
        self.layout.operator(
            operators.convert_to_bone_anim.ArmatureDeformObjects.bl_idname)


class TarvalleyMenu(Menu):
    """A menu for any registered functionality that should be accessible in any context"""
    bl_idname = "TOPBAR_MT_Tarvalley"
    bl_label = "Tarvalley"

    def draw(self, context):
        """Draw the menu"""
        self.layout.operator(
            operators.auto_smooth_normals_to_edge_split.AutoSmoothNormalsToEdgeSplit.bl_idname)
        self.layout.menu(ConvertToBoneAnimMenu.bl_idname)
        self.layout.operator(
            operators.skin_mesh_to_bone.SkinMeshToBone.bl_idname)


def context_menu_draw(self, context):
    """Adds operator to menu"""
    self.layout.separator()
    # self.layout.operator_context = 'INVOKE_DEFAULT'
    self.layout.operator(operators.shade_auto.ShadeAuto.bl_idname)


def menu_draw(self, context):
    """Draws the Tarvalley menu"""
    self.layout.menu(TarvalleyMenu.bl_idname,
                     icon_value=get_icon("tarvalley_logo_icon"))


__classes__ = (
    TarvalleyMenu,
    ConvertToBoneAnimMenu,
)


def register():
    """Load the icons, register the menus and add them to the Blender UI"""
    global __icon_manager__  # pylint: disable=global-statement
    if __icon_manager__ is None:
        __icon_manager__ = IconManager()

    for cls in __classes__:
        register_class(cls)

    bpy.types.VIEW3D_MT_object_context_menu.append(context_menu_draw)
    bpy.types.TOPBAR_MT_editor_menus.append(menu_draw)


def unregister():
    """Remove the menus and Load the icons, register the menus and add them to the Blender UI"""
    for cls in reversed(__classes__):
        unregister_class(cls)

    global __icon_manager__  # pylint: disable=global-statement
    __icon_manager__.unregister()
    __icon_manager__ = None

    bpy.types.VIEW3D_MT_object_context_menu.remove(context_menu_draw)
    bpy.types.TOPBAR_MT_editor_menus.remove(menu_draw)
