"""Blender add-on preferences for the Tarvalley Addon"""

import bpy
from . import ADDON_NAME
from ..vendor.addon_updater import addon_updater_ops


class TarvalleyAddonPreferences(bpy.types.AddonPreferences):
    """Preferences class for the Tarvalley Addon"""

    bl_idname = ADDON_NAME
    bl_label = "Source Location"
    bl_region_type = 'UI'
    bl_category = 'Tarvalley'

    # addon updater preferences
    auto_check_update: bpy.props.BoolProperty(
        name="Auto-check for Update",
        description="If enabled, auto-check for updates using an interval",
        default=False,
    )
    updater_intrval_months: bpy.props.IntProperty(
        name='Months',
        description="Number of months between checking for updates",
        default=0,
        min=0
    )
    updater_intrval_days: bpy.props.IntProperty(
        name='Days',
        description="Number of days between checking for updates",
        default=7,
        min=0,
        max=31
    )
    updater_intrval_hours: bpy.props.IntProperty(
        name='Hours',
        description="Number of hours between checking for updates",
        default=0,
        min=0,
        max=23
    )
    updater_intrval_minutes: bpy.props.IntProperty(
        name='Minutes',
        description="Number of minutes between checking for updates",
        default=0,
        min=0,
        max=59
    )

    def draw(self, context):
        """Draws the preferences"""

        addon_updater_ops.update_settings_ui(self, context)


REGISTER_CLASSES = (
    TarvalleyAddonPreferences,
)
